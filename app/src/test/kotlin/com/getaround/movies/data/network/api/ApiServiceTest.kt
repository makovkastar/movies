package com.getaround.movies.data.network.api

import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.testutil.assertNotNull
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.StandardCharsets

class ApiServiceTest {

    private lateinit var apiService: ApiService

    @Rule
    @JvmField
    val mockWebServer = MockWebServer()

    @Before
    fun setUp() {
        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Test
    fun `getNowPlayingMovies sends correct API request`() {
        enqueueResponse("get-now-playing.json")

        apiService.getNowPlayingMovies(1).execute()

        val recordedRequest = mockWebServer.takeRequest()

        assertThat(recordedRequest.path, equalTo("/movie/now_playing?page=1"))
    }

    @Test
    fun `searchMovies sends correct API request`() {
        enqueueResponse("search-movies.json")

        apiService.searchMovies("Bohemian Rhapsody", 1).execute()

        val recordedRequest = mockWebServer.takeRequest()

        assertThat(recordedRequest.path,
            equalTo("/search/movie?query=Bohemian%20Rhapsody&page=1"))
    }

    @Test
    fun `getNowPlayingMovies API response is properly converted`() {
        enqueueResponse("get-now-playing.json")

        val response = apiService.getNowPlayingMovies(1).execute()
        val movieListDto = response.body()

        assertNotNull(movieListDto)
        assertThat(movieListDto.page, equalTo(1))
        assertThat(movieListDto.totalPages, equalTo(50))
        assertThat(movieListDto.results.size, equalTo(4))
        assertThat(movieListDto.results, equalTo(listOf(
            MovieDto(
                id = 297802,
                title = "Aquaman",
                overview = "Arthur Curry learns that he is the heir to the underwater kingdom of Atlantis, and must step forward to lead his people and be a hero to the world.",
                posterPath = "/i2dF9UxOeb77CAJrOflj0RpqJRF.jpg",
                backdropPath = "/5A2bMlLfJrAfX9bqAibOL2gCruF.jpg"
            ),
            MovieDto(
                id = 424783,
                title = "Bumblebee",
                overview = "On the run in the year 1987, Bumblebee finds refuge in a junkyard in a small Californian beach town. Charlie, on the cusp of turning 18 and trying to find her place in the world, discovers Bumblebee, battle-scarred and broken.  When Charlie revives him, she quickly learns this is no ordinary yellow VW bug.",
                posterPath = "/sG6n4ei1F0kVQtTs3fAjDghngpa.jpg",
                backdropPath = "/urPYbqQnMMu5Ug09nI0KAGabbDX.jpg"
            ),
            MovieDto(
                id = 338952,
                title = "Fantastic Beasts: The Crimes of Grindelwald",
                overview = "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.",
                posterPath = "/kQKcbJ9uYkTQql2R8L4jTUz7l90.jpg",
                backdropPath = "/wDN3FIcQQ1HI7mz1OOKYHSQtaiE.jpg"
            ),
            MovieDto(
                id = 335983,
                title = "Venom",
                overview = "Investigative journalist Eddie Brock attempts a comeback following a scandal, but accidentally becomes the host of an alien symbiote that gives him a violent super alter-ego: Venom. Soon, he must rely on his newfound powers to protect the world from a shadowy organization looking for a symbiote of their own.",
                posterPath = "/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg",
                backdropPath = "/VuukZLgaCrho2Ar8Scl9HtV3yD.jpg"
            )
        )))
    }

    @Test
    fun `searchMovies API response is properly converted`() {
        enqueueResponse("search-movies.json")

        val response = apiService.getNowPlayingMovies(1).execute()
        val movieListDto = response.body()

        assertNotNull(movieListDto)
        assertThat(movieListDto.page, equalTo(1))
        assertThat(movieListDto.totalPages, equalTo(1))
        assertThat(movieListDto.results.size, equalTo(2))
        assertThat(movieListDto.results, equalTo(listOf(
            MovieDto(
                id = 424694,
                title = "Bohemian Rhapsody",
                overview = "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.",
                posterPath = "/lHu1wtNaczFPGFDTrjCSzeLPTKN.jpg",
                backdropPath = "/pbXgLEYh8rlG2Km5IGZPnhcnuSz.jpg"
            ),
            MovieDto(
                id = 397867,
                title = "The Story of Bohemian Rhapsody",
                overview = "The story behind the epic Queen single.",
                posterPath = "/dh5T5o6dUyjZnYZuufH7ZV32SJP.jpg",
                backdropPath = null
            )
        )))
    }

    private fun enqueueResponse(fileName: String) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        mockWebServer.enqueue(MockResponse()
            .setBody(source.readString(StandardCharsets.UTF_8)))
    }
}
