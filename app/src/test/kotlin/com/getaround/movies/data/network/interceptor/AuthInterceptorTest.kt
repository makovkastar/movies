package com.getaround.movies.data.network.interceptor

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Rule
import org.junit.Test

class AuthInterceptorTest {

    @Rule
    @JvmField
    val mockWebServer = MockWebServer()

    @Test
    fun `intercept adds API key to request`() {
        mockWebServer.enqueue(MockResponse())

        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(AuthInterceptor("TEST_API_KEY"))
            .build()
        val request = Request.Builder()
            .url(mockWebServer.url("/"))
            .build()
        okHttpClient.newCall(request).execute()

        val recordedRequest = mockWebServer.takeRequest()

        assertThat(recordedRequest.requestUrl.queryParameter(
            AuthInterceptor.QueryParams.API_KEY), equalTo("TEST_API_KEY"))
    }
}