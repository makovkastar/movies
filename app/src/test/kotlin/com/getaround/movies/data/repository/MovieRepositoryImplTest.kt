package com.getaround.movies.data.repository

import com.getaround.movies.data.network.api.ApiService
import com.getaround.movies.data.network.dto.MovieListDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.anyString
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response
import java.net.ConnectException

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieRepositoryImplTest {

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var call: Call<MovieListDto>

    @Mock
    private lateinit var movieListDto: MovieListDto

    private lateinit var movieRepo: MovieRepositoryImpl

    @Before
    fun setUp() {
        movieRepo = MovieRepositoryImpl(
            apiService, Dispatchers.Unconfined)
    }

    @Test
    fun `getNowPlayingMovies executes correct API call`() = runBlocking {
        `when`(apiService.getNowPlayingMovies(anyInt())).thenReturn(call)
        `when`(call.execute()).thenReturn(Response.success(movieListDto))

        movieRepo.getNowPlayingMovies(1)

        verify(apiService).getNowPlayingMovies(1)
        verify(call).execute()

        verifyNoMoreInteractions(apiService)
        verifyNoMoreInteractions(call)
    }

    @Test
    fun `searchMovies executes correct API call`() = runBlocking {
        `when`(apiService.searchMovies(anyString(), anyInt())).thenReturn(call)
        `when`(call.execute()).thenReturn(Response.success(movieListDto))

        movieRepo.searchMovies("Bohemian Rhapsody", 1)

        verify(apiService).searchMovies("Bohemian Rhapsody", 1)
        verify(call).execute()

        verifyNoMoreInteractions(apiService)
        verifyNoMoreInteractions(call)
    }

    @Test
    fun `getNowPlayingMovies returns successful result when API response is successful`() = runBlocking {
        `when`(apiService.getNowPlayingMovies(anyInt())).thenReturn(call)
        `when`(call.execute()).thenReturn(Response.success(movieListDto))

        val getNowPlayingMoviesResult = movieRepo.getNowPlayingMovies(1)

        assertThat(getNowPlayingMoviesResult, instanceOf(
            GetNowPlayingMoviesResult.Success::class.java))
        assertThat((getNowPlayingMoviesResult as
                GetNowPlayingMoviesResult.Success).data, equalTo(movieListDto))
    }

    @Test
    fun `getNowPlayingMovies returns error result when API response has error`() = runBlocking {
        `when`(apiService.getNowPlayingMovies(anyInt())).thenReturn(call)
        `when`(call.execute()).thenReturn(Response.error(401, ResponseBody.create(
            MediaType.parse("application/json"), "Invalid API key")))

        val getNowPlayingMoviesResult = movieRepo.getNowPlayingMovies(1)

        assertThat(getNowPlayingMoviesResult, instanceOf(
            GetNowPlayingMoviesResult.Error::class.java))
    }

    @Test
    fun `getNowPlayingMovies returns error result when API response body is null`() = runBlocking {
        `when`(apiService.getNowPlayingMovies(anyInt())).thenReturn(call)
        `when`(call.execute()).thenReturn(Response.success(null))

        val getNowPlayingMoviesResult = movieRepo.getNowPlayingMovies(1)

        assertThat(getNowPlayingMoviesResult, instanceOf(
            GetNowPlayingMoviesResult.Error::class.java))
    }

    @Test
    fun `getNowPlayingMovies returns error result when there is connection problem`() = runBlocking {
        `when`(apiService.getNowPlayingMovies(anyInt())).thenReturn(call)
        `when`(call.execute()).thenThrow(ConnectException("Failed to connect"))

        val getNowPlayingMoviesResult = movieRepo.getNowPlayingMovies(1)

        assertThat(getNowPlayingMoviesResult, instanceOf(
            GetNowPlayingMoviesResult.Error::class.java))
    }

    // TODO: Implement tests for searchMovies()
}
