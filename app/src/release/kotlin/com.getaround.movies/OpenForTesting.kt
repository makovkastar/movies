package com.getaround.movies

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
