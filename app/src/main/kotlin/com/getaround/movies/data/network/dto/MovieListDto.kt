package com.getaround.movies.data.network.dto

import com.google.gson.annotations.SerializedName

data class MovieListDto(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<MovieDto>,
    @SerializedName("total_pages") val totalPages: Int
)