package com.getaround.movies.data.repository

import com.getaround.movies.data.network.dto.MovieListDto

interface MovieRepository {
    suspend fun getNowPlayingMovies(page: Int): GetNowPlayingMoviesResult

    suspend fun searchMovies(query: String, page: Int): SearchMoviesResult
}

sealed class GetNowPlayingMoviesResult {
    class Success(val data: MovieListDto) : GetNowPlayingMoviesResult()
    object Error : GetNowPlayingMoviesResult()
}

sealed class SearchMoviesResult {
    class Success(val data: MovieListDto) : SearchMoviesResult()
    object Error : SearchMoviesResult()
}