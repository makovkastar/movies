package com.getaround.movies.data.network.api

import com.getaround.movies.data.network.dto.MovieListDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("movie/now_playing")
    fun getNowPlayingMovies(@Query("page") page: Int): Call<MovieListDto>

    @GET("search/movie")
    fun searchMovies(
        @Query("query") query: String,
        @Query("page") page: Int
    ): Call<MovieListDto>
}
