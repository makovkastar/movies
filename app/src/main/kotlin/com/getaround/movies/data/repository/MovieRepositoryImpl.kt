package com.getaround.movies.data.repository

import com.getaround.movies.data.network.api.ApiService
import com.getaround.movies.injection.qualifier.BgContext
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @BgContext private val bgContext: CoroutineContext
) : MovieRepository {

    override suspend fun getNowPlayingMovies(page: Int) = withContext(bgContext) {
        try {
            val response = apiService.getNowPlayingMovies(page).execute()

            if (response.isSuccessful) {
                response.body()?.let {
                    return@withContext GetNowPlayingMoviesResult.Success(it)
                }
            }
        } catch (e: IOException) {
            Timber.e(e, "Unable to get now playing movies")
        }

        return@withContext GetNowPlayingMoviesResult.Error
    }

    override suspend fun searchMovies(query: String, page: Int) = withContext(bgContext) {
        try {
            val response = apiService.searchMovies(query, page).execute()

            if (response.isSuccessful) {
                response.body()?.let {
                    return@withContext SearchMoviesResult.Success(it)
                }
            }
        } catch (e: IOException) {
            Timber.e(e, "Unable to search movies")
        }

        return@withContext SearchMoviesResult.Error
    }
}
