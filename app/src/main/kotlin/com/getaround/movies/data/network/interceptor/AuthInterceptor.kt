package com.getaround.movies.data.network.interceptor

import android.support.annotation.VisibleForTesting
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val apiKey: String): Interceptor {

    @VisibleForTesting
    object QueryParams {
        const val API_KEY = "api_key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url()

        val newUrl = originalUrl.newBuilder()
            .addQueryParameter(QueryParams.API_KEY, apiKey)
            .build()
        val newRequest = originalRequest.newBuilder()
            .url(newUrl)
            .build()

        return chain.proceed(newRequest)
    }
}
