package com.getaround.movies.injection.module

import com.getaround.movies.data.repository.MovieRepository
import com.getaround.movies.data.repository.MovieRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun bindMovieRepository(movieRepo: MovieRepositoryImpl): MovieRepository
}
