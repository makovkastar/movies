package com.getaround.movies.injection.module

import com.getaround.movies.ui.list.MovieListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {
    @ContributesAndroidInjector
    fun movieListActivity(): MovieListActivity
}
