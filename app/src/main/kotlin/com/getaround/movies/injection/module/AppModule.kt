package com.getaround.movies.injection.module

import com.getaround.movies.injection.qualifier.BgContext
import com.getaround.movies.injection.qualifier.UiContext
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

@Module
object AppModule {
    @Provides
    @UiContext
    @JvmStatic
    fun provideCoroutineUiContext(): CoroutineContext = Dispatchers.Main

    @Provides
    @BgContext
    @JvmStatic
    fun provideCoroutineBgContext(): CoroutineContext = Dispatchers.Default
}