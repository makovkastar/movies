package com.getaround.movies.injection.module

import com.getaround.movies.BuildConfig
import com.getaround.movies.data.network.api.ApiService
import com.getaround.movies.data.network.interceptor.AuthInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object NetworkModule {

    private const val API_BASE_URL = "https://api.themoviedb.org/3/"

    private const val API_KEY = "45b63fd44cc2a000444038d5a75aa89c"

    @Provides
    @JvmStatic
    fun provideAuthInterceptor(): AuthInterceptor = AuthInterceptor(API_KEY)

    @Provides
    @JvmStatic
    fun provideOkHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(authInterceptor)
            .build()
    }

    @Provides
    @JvmStatic
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @JvmStatic
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        factory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(API_BASE_URL)
            .addConverterFactory(factory)
            .validateEagerly(BuildConfig.DEBUG)
            .build()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}
