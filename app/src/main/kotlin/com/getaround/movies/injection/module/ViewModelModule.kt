package com.getaround.movies.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.getaround.movies.injection.ViewModelFactory
import com.getaround.movies.injection.ViewModelKey
import com.getaround.movies.ui.list.MovieListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    fun bindLoginViewModel(viewModel: MovieListViewModel): ViewModel
}
