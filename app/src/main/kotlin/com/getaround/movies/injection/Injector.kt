package com.getaround.movies.injection

import android.app.Activity
import android.app.Application
import android.os.Bundle
import dagger.android.AndroidInjection

object Injector {

    fun init(app: Application) {
        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                if (activity is Injectable) {
                    AndroidInjection.inject(activity)
                }
            }

            override fun onActivityPaused(activity: Activity) {}

            override fun onActivityResumed(activity: Activity) {}

            override fun onActivityStarted(activity: Activity) {}

            override fun onActivityDestroyed(activity: Activity) {}

            override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle?) {}

            override fun onActivityStopped(activity: Activity) {}
        })
    }
}
