package com.getaround.movies.util

import android.app.Activity
import android.support.annotation.StringRes
import android.widget.Toast

fun Activity.showToast(@StringRes messageRes: Int) =
    Toast.makeText(this, messageRes, Toast.LENGTH_SHORT).show()
