package com.getaround.movies.ui.list.listener

import com.getaround.movies.data.network.dto.MovieDto

interface MovieClickListener {
    fun onMovieClicked(movie: MovieDto)
}