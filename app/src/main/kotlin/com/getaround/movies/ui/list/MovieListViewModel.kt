package com.getaround.movies.ui.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import com.getaround.movies.OpenForTesting
import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.movies.data.repository.GetNowPlayingMoviesResult
import com.getaround.movies.data.repository.MovieRepository
import com.getaround.movies.data.repository.SearchMoviesResult
import com.getaround.movies.injection.qualifier.UiContext
import com.getaround.movies.ui.common.SingleLiveEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@OpenForTesting
class MovieListViewModel @Inject constructor(
    private val movieRepo: MovieRepository,
    @UiContext private val uiContext: CoroutineContext
) : ViewModel() {

    private companion object {
        const val SEARCH_QUERY_THRESHOLD = 3
        const val START_PAGE = 1
    }

    private enum class Mode {
        BROWSE, SEARCH
    }

    private var mode = Mode.BROWSE

    private var pageToLoad = START_PAGE

    private var searchQuery: String = ""

    val isLoading = ObservableBoolean(false)

    val movies = MutableLiveData<List<MovieDto>>()

    val scrollToTop = SingleLiveEvent<Unit>()

    val showErrorToast = SingleLiveEvent<Unit>()

    val navigateToMovieDetails = SingleLiveEvent<MovieDto>()

    // TODO: Limit scope of coroutine to ViewModel
    // https://chris.banes.me/talks/2018/android-suspenders/
    fun loadNowPlayingMovies() = GlobalScope.launch(uiContext) {
        isLoading.set(true)

        val result = movieRepo.getNowPlayingMovies(pageToLoad)

        when (result) {
            is GetNowPlayingMoviesResult.Success -> {
                if (pageToLoad == START_PAGE) {
                    movies.value = result.data.results
                    scrollToTop.call()
                } else {
                    movies.value =
                        mergeResults(result.data.results)
                }
                pageToLoad++
            }
            is GetNowPlayingMoviesResult.Error -> {
                showErrorToast.call()
            }
        }

        isLoading.set(false)
    }

    private fun mergeResults(results: List<MovieDto>) =
        movies.value.orEmpty() + results

    fun onQueryTextChanged(query: String) {
        pageToLoad = START_PAGE
        searchQuery = query

        if (query.isEmpty()) {
            mode = Mode.BROWSE
            loadNowPlayingMovies()
        } else if (query.isSearchable()) {
            mode = Mode.SEARCH
            searchMovies(query)
        }
    }

    fun onMovieClicked(movie: MovieDto) {
        navigateToMovieDetails.value = movie
    }

    fun onLoadMoreClicked() {
        when (mode) {
            Mode.BROWSE -> {
                loadNowPlayingMovies()
            }
            Mode.SEARCH -> {
                searchMovies(searchQuery)
            }
        }
    }

    private fun String.isSearchable() = this.length >= SEARCH_QUERY_THRESHOLD

    private fun searchMovies(query: String) = GlobalScope.launch(uiContext) {
        isLoading.set(true)

        val result = movieRepo.searchMovies(query, pageToLoad)

        when (result) {
            is SearchMoviesResult.Success -> {
                if (pageToLoad == START_PAGE) {
                    movies.value = result.data.results
                } else {
                    movies.value =
                        mergeResults(result.data.results)
                }
                pageToLoad++
            }
            is SearchMoviesResult.Error -> {
                showErrorToast.call()
            }
        }

        isLoading.set(false)
    }
}
