package com.getaround.movies.ui.list.listener

interface LoadMoreClickListener {
    fun onLoadMoreClicked()
}