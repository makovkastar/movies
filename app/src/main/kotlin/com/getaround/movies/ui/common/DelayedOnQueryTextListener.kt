package com.getaround.movies.ui.common

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Message
import android.support.v7.widget.SearchView

abstract class DelayedOnQueryTextListener : SearchView.OnQueryTextListener {

    private companion object {
        const val MESSAGE_QUERY_TEXT_CHANGE = 100
        const val QUERY_TEXT_CHANGE_DELAY_MS = 500L
    }

    @SuppressLint("HandlerLeak")
    private val handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            this@DelayedOnQueryTextListener
                .delayedOnQueryTextChange(msg.obj as String)
        }
    }

    override fun onQueryTextSubmit(query: String) = false

    override fun onQueryTextChange(query: String): Boolean {
        handler.removeMessages(MESSAGE_QUERY_TEXT_CHANGE)
        handler.sendMessageDelayed(
            handler.obtainMessage(
                MESSAGE_QUERY_TEXT_CHANGE, query
            ), QUERY_TEXT_CHANGE_DELAY_MS
        )
        return true
    }

    abstract fun delayedOnQueryTextChange(query: String)
}
