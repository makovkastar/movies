package com.getaround.movies.ui.details

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.getaround.movies.R
import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.movies.databinding.MovieDetailsActivityBinding

class MovieDetailsActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_MOVIE = "EXTRA_MOVIE"

        fun getStartIntent(context: Context, movie: MovieDto) =
            Intent(context, MovieDetailsActivity::class.java).apply {
                putExtra(EXTRA_MOVIE, movie)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val dataBinding: MovieDetailsActivityBinding = DataBindingUtil
            .setContentView(this, R.layout.activity_movie_details)
        dataBinding.setLifecycleOwner(this)
        dataBinding.movie = intent.getParcelableExtra(EXTRA_MOVIE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}
