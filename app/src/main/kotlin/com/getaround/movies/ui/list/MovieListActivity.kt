package com.getaround.movies.ui.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import com.getaround.movies.R
import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.movies.databinding.MovieListActivityBinding
import com.getaround.movies.injection.Injectable
import com.getaround.movies.ui.common.DelayedOnQueryTextListener
import com.getaround.movies.ui.details.MovieDetailsActivity
import com.getaround.movies.ui.list.adapter.MovieListAdapter
import com.getaround.movies.util.showToast
import kotlinx.android.synthetic.main.activity_movie_list.*
import javax.inject.Inject

class MovieListActivity : AppCompatActivity(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MovieListViewModel

    private lateinit var recyclerViewAdapter: MovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MovieListViewModel::class.java)

        val dataBinding: MovieListActivityBinding = DataBindingUtil
            .setContentView(this, R.layout.activity_movie_list)
        dataBinding.setLifecycleOwner(this)
        dataBinding.viewModel = viewModel

        setupRecyclerView()
        setupSwipeRefreshLayout()

        viewModel.movies.observe(this, Observer {
            renderMovies(checkNotNull(it))
        })

        viewModel.scrollToTop.observe(this, Observer {
            recycler_view_movies.scrollToPosition(0)
        })

        viewModel.showErrorToast.observe(this, Observer {
            showToast(R.string.toast_now_playing_movies_error)
        })

        viewModel.navigateToMovieDetails.observe(this, Observer {
            startMovieDetailsActivity(checkNotNull(it))
        })

        if (savedInstanceState == null) {
            viewModel.loadNowPlayingMovies()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_movie_list, menu)

        val searchItem = menu.findItem(R.id.item_search)
        val searchView = searchItem.actionView as SearchView

        // Set max width to a really big value to force
        // SearchView to expand to the width of Toolbar:
        // https://stackoverflow.com/a/37162149/1552622
        searchView.maxWidth = Integer.MAX_VALUE

        searchView.setOnQueryTextListener(object : DelayedOnQueryTextListener() {
            override fun delayedOnQueryTextChange(query: String) {
                viewModel.onQueryTextChanged(query)
            }
        })

        return true
    }

    private fun setupRecyclerView() {
        val spanCount = resources.getInteger(
            R.integer.grid_movies_column_count)

        recyclerViewAdapter = MovieListAdapter(
            MovieListAdapterListener(), spanCount)

        val gridLayoutManager = GridLayoutManager(this, spanCount).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int) =
                    recyclerViewAdapter.getSpanSize(position)
            }
        }

        recycler_view_movies.apply {
            adapter = recyclerViewAdapter
            layoutManager = gridLayoutManager
        }
    }

    private fun setupSwipeRefreshLayout() {
        swipe_refresh_layout.isEnabled = false
        swipe_refresh_layout.setColorSchemeColors(
            ContextCompat.getColor(this, R.color.accent))
    }

    private fun renderMovies(movies: List<MovieDto>) {
        recyclerViewAdapter.submitList(movies)
    }

    private fun startMovieDetailsActivity(movie: MovieDto) {
        startActivity(MovieDetailsActivity.getStartIntent(this, movie))
    }

    private inner class MovieListAdapterListener : MovieListAdapter.Listener {

        override fun onMovieClicked(movie: MovieDto) {
            viewModel.onMovieClicked(movie)
        }

        override fun onLoadMoreClicked() {
            viewModel.onLoadMoreClicked()
        }
    }
}
