package com.getaround.movies.ui.list.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.movies.databinding.LoadMoreListItemBinding
import com.getaround.movies.databinding.MovieListItemBinding
import com.getaround.movies.ui.databinding.DataBindingViewHolder
import com.getaround.movies.ui.list.listener.LoadMoreClickListener
import com.getaround.movies.ui.list.listener.MovieClickListener

class MovieListAdapter(
    private val listener: Listener,
    private val spanCount: Int
) : ListAdapter<MovieDto, DataBindingViewHolder>(DiffCallback()) {

    private enum class ItemView(val type: Int) {
        MOVIE(0), LOAD_MORE(1)
    }

    interface Listener : MovieClickListener, LoadMoreClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ItemView.MOVIE.type -> {
                val dataBinding = MovieListItemBinding.inflate(
                    layoutInflater, parent, false)
                dataBinding.listener = listener
                DataBindingViewHolder(dataBinding)
            }
            ItemView.LOAD_MORE.type -> {
                val dataBinding = LoadMoreListItemBinding.inflate(
                    layoutInflater, parent, false)
                dataBinding.listener = listener
                DataBindingViewHolder(dataBinding)
            }
            else -> {
                throw IllegalArgumentException("Unknown view type: $viewType")
            }
        }
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder, position: Int) {
        if (holder.itemViewType == ItemView.MOVIE.type) {
            holder.bind(getItem(position))
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1) {
            ItemView.LOAD_MORE.type
        } else {
            ItemView.MOVIE.type
        }
    }

    fun getSpanSize(position: Int): Int {
        val viewType = getItemViewType(position)
        return when (viewType) {
            ItemView.MOVIE.type -> 1
            ItemView.LOAD_MORE.type -> spanCount
            else -> {
                throw IllegalArgumentException("Unknown view type: $viewType")
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<MovieDto>() {

        override fun areItemsTheSame(oldItem: MovieDto, newItem: MovieDto) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: MovieDto, newItem: MovieDto) =
            oldItem == newItem
    }
}
