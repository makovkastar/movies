package com.getaround.movies.ui.databinding

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import com.getaround.movies.BR

open class DataBindingViewHolder(
    private val dataBinding: ViewDataBinding
) : RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(obj: Any) {
        dataBinding.setVariable(BR.obj, obj)
        dataBinding.executePendingBindings()
    }
}
