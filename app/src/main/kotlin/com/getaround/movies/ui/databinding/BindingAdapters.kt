package com.getaround.movies.ui.databinding

import android.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso

@BindingAdapter("gone")
fun gone(view: View, gone: Boolean) {
    view.visibility = if (gone) View.GONE else View.VISIBLE
}

@BindingAdapter("moviePoster")
fun moviePoster(imageView: ImageView, imagePath: String?) {
    Picasso.get()
        .load("https://image.tmdb.org/t/p/w500$imagePath")
        .into(imageView)
}

@BindingAdapter("movieBackdrop")
fun movieBackdrop(imageView: ImageView, imagePath: String?) {
    Picasso.get()
        .load("https://image.tmdb.org/t/p/w1280$imagePath")
        .into(imageView)
}
