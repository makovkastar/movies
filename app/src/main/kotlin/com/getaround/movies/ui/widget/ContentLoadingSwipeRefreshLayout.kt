package com.getaround.movies.ui.widget

import android.content.Context
import android.os.SystemClock
import android.support.v4.widget.SwipeRefreshLayout
import android.util.AttributeSet

class ContentLoadingSwipeRefreshLayout(
    context: Context,
    attrs: AttributeSet
) : SwipeRefreshLayout(context, attrs) {

    private companion object {
        const val MIN_SHOW_TIME_MS = 200L
        const val MIN_DELAY_MS = 500L
        const val TIME_NOT_SET = -1L
    }

    private var startTime = TIME_NOT_SET

    private var postedHide = false

    private var postedShow = false

    private var dismissed = false

    private val delayedShow = Runnable {
        postedShow = false
        if (!dismissed) {
            startTime = SystemClock.elapsedRealtime()
            super.setRefreshing(true)
        }
    }

    private val delayedHide = Runnable {
        postedHide = false
        startTime = TIME_NOT_SET
        super.setRefreshing(false)
    }

    override fun setRefreshing(refreshing: Boolean) {
        if (refreshing) {
            showRefreshing()
        } else {
            hideRefreshing()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        removeCallbacks()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeCallbacks()
    }

    private fun showRefreshing() {
        startTime = TIME_NOT_SET
        dismissed = false
        removeCallbacks(delayedHide)
        if (!postedShow) {
            postDelayed(delayedShow, MIN_DELAY_MS)
            postedShow = true
        }
    }

    private fun hideRefreshing() {
        dismissed = true
        removeCallbacks(delayedShow)
        val timeDiff = System.currentTimeMillis() - startTime
        if (timeDiff >= MIN_SHOW_TIME_MS || startTime == TIME_NOT_SET) {
            // The refreshing indicator has been shown long enough
            // or was not shown yet. If it wasn't shown yet,
            // it will just never be shown.
            super.setRefreshing(false)
        } else {
            // The refreshing indicator is shown, but not long enough,
            // so put a delayed message in to hide it when its been
            // shown long enough.
            if (!postedHide) {
                postDelayed(delayedHide, MIN_SHOW_TIME_MS - timeDiff)
                postedHide = true
            }
        }
    }

    private fun removeCallbacks() {
        removeCallbacks(delayedShow)
        removeCallbacks(delayedHide)
    }
}
