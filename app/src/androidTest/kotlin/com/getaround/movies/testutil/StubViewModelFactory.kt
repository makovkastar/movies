package com.getaround.movies.testutil

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class StubViewModelFactory(private val viewModel: ViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(viewModelClass: Class<T>): T {
        if (viewModelClass.isAssignableFrom(viewModel.javaClass)) {
            @Suppress("UNCHECKED_CAST")
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown view model class $viewModelClass")
    }
}
