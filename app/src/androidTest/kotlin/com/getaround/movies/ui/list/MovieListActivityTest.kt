package com.getaround.movies.ui.list

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.databinding.ObservableBoolean
import android.os.SystemClock
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.isInternal
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import android.support.test.runner.intercepting.SingleActivityFactory
import com.getaround.movies.R
import com.getaround.movies.data.network.dto.MovieDto
import com.getaround.movies.testutil.IntentsTestRule
import com.getaround.movies.testutil.StubViewModelFactory
import com.getaround.movies.testutil.SwipeRefreshLayoutMatchers.isRefreshing
import com.getaround.movies.testutil.ToastMatchers.isToast
import com.getaround.movies.ui.common.SingleLiveEvent
import com.getaround.movies.ui.details.MovieDetailsActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@RunWith(AndroidJUnit4::class)
class MovieListActivityTest {

    @Rule
    @JvmField
    val activityRule = IntentsTestRule(MovieListActivityFactory(), true, false)

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var viewModel: MovieListViewModel

    @Mock
    private lateinit var movie: MovieDto

    private val isLoading = ObservableBoolean()
    private val movies = MutableLiveData<List<MovieDto>>()
    private val showErrorToast = SingleLiveEvent<Unit>()
    private val navigateToMovieDetails = SingleLiveEvent<MovieDto>()

    @Before
    fun setUp() {
        `when`(viewModel.isLoading).thenReturn(isLoading)
        `when`(viewModel.movies).thenReturn(movies)
        `when`(viewModel.showErrorToast).thenReturn(showErrorToast)
        `when`(viewModel.navigateToMovieDetails).thenReturn(navigateToMovieDetails)

        activityRule.launchActivity(null)
    }

    @Test
    fun isLoadingBinding_True() {
        isLoading.set(true)

        // ContentLoadingSwipeRefreshLayout waits a minimum time to be dismissed
        // before showing the refreshing indicator, so wait before checking it.
        SystemClock.sleep(1000)

        onView(withId(R.id.swipe_refresh_layout))
            .check(matches(isRefreshing()))
    }

    @Test
    fun showErrorToast_ShowsToastWithCorrectText() {
        activityRule.runOnUiThread {
            showErrorToast.call()
        }

        onView(withText(R.string.toast_now_playing_movies_error))
            .inRoot(isToast()).check(matches(isDisplayed()))
    }

    @Test
    fun navigateToMovieDetails_StartsMovieDetailsActivity() {
        intending(isInternal()).respondWith(
            ActivityResult(Activity.RESULT_OK, null))

        activityRule.runOnUiThread {
            navigateToMovieDetails.value = movie
        }

        intended(hasComponent(MovieDetailsActivity::class.java.name))
    }

    inner class MovieListActivityFactory : SingleActivityFactory<MovieListActivity>(
        MovieListActivity::class.java) {

        override fun create(intent: Intent?) = MovieListActivity().apply {
            viewModelFactory = StubViewModelFactory(viewModel)
        }
    }
}